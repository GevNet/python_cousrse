def something(diction):
    return {key: value for value, key in diction.items()}


if __name__ == "__main__":
    print(something({'Petr': '546810', 'Katya': '241815'}))

