def lists_to_dict(date_list, int_list):
    return {date_list[i]: int_list[i] for i in range(len(date_list))}


if __name__ == "__main__":
    print(lists_to_dict(['2017-03-01', '2017-03-02'], [55.7, 55.2]))
