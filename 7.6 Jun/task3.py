def rates(exchange_rate):
    a = min([i for i in exchange_rate.values()])
    return {key: value for key, value in exchange_rate.items() if value == a}


if __name__ == "__main__":
    print(rates({'Sberbank': 55.8, 'VTB24': 53.91}))
