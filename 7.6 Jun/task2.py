def arabica_to_rome(some_int):
    a = ''
    b = ''
    arabic_rome = {
        1000: 'M',
        900: 'CM',
        500: 'D',
        400: 'CD',
        100: 'C',
        90: 'XC',
        50: 'L',
        40: 'XL',
        10: 'X',
        9: 'IX',
        5: 'V',
        4: 'IV',
        1: 'I',
    }
    for key, value in arabic_rome.items():
        a += some_int // key * value
        some_int %= key
    return a


if __name__ == "__main__":
    print(arabica_to_rome(6))