import re
import string


def popularity(some_text):
    dict_pop_words = {}
    words = re.findall(r"\w+", some_text)
    for i in words:
        dict_pop_words[i] = [words.count(i)]
    dict_pop_letter = {key: some_text.count(key) for key in some_text if key in string.ascii_letters}
    return dict_pop_letter, dict_pop_words


if __name__ == "__main__":
    print(popularity("hello, word of word"))