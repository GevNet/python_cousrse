import asyncio_extras


def x_o(data):
    who_win = 'draw'
    x = [i for i in data]
    s = ''.join(x)
    if s[0] == s[1] == s[2]:
        who_win = s[0]
    if s[3] == s[4] == s[5]:
        who_win = s[3]
    if s[6] == s[7] == s[8]:
        who_win = s[6]
    if s[0] == s[3] == s[6]:
        who_win = s[0]
    if s[1] == s[4] == s[7]:
        who_win = s[1]
    if s[2] == s[5] == s[8]:
        who_win = s[2]
    if s[0] == s[4] == s[8]:
        who_win = s[0]
    if s[2] == s[4] == s[6]:
        who_win = s[2]
    return who_win + " wins" if who_win != "draw" else who_win


if __name__ == "__main__":
    print(x_o(["0XX",
                "00X",
                "X00"]))