def lazy_gen(n):
    for i in range(n+1):
        if i == 0:
            yield -19
        elif i % 3:
            yield 45
        elif i % 5:
            yield i / 5 + 93
        else:
            yield i / 2


print(list(lazy_gen(7)))