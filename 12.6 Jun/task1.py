class Friends:

    def __init__(self, connections: list):
        self.connections = connections

    @classmethod
    def chk(cls, connections: list, n_conn: set) -> bool:
        for c in connections:
            if n_conn <= c:
                return True

    def add(self, new_conn: set) -> bool:
        if not Friends.chk(self.connections, new_conn):
            self.connections.append(new_conn)
            return True
        return False

    def removed(self, r_conn: set) -> bool:
        if Friends.chk(self.connections, r_conn):
            self.connections.remove(r_conn)
            return True
        return False

    def names(self) -> set:
        s = set()
        for sets in self.connections:
            for names in sets:
                s.add(names)
        return s

    def connected(self, name: str) -> set:
        s = set()
        for sets in self.connections:
            if name in sets:
                s.update((sets - set(name)))
        return s


if __name__ == '__main__':
    f = Friends([{"1", "2"}, {"3", "1"}])
    print(f.add({"1", "3"}))
    print(f.add({"4", "5"}))
    f = Friends([{"a", "b"}, {"b", "c"}, {"c", "a"}])
    print(f.connected("a"))
    print(f.connected("d"))
    print(f.removed({"c", "a"}))
    print(f.connected("c"))
