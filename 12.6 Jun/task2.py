class Student:

    def __init__(self, name: str, group: str, grades: dict) -> None:
        self.name = name
        self.group = group
        self.grades = grades

    @classmethod
    def get_score_by_grade(cls, grades):
        score = 0
        num = 0
        for k in grades:
            score += sum(grades[k])
            num += len(grades[k])
        return score / num

    def get_average_score_student(self) -> str:
        return f'{self.name} AVG score = {Student.get_score_by_grade(grades=self.grades)}'

    def get_grades(self):
        return self.grades

    @staticmethod
    def get_average_score_group(list_of_students: list, group_number: str) -> str:
        score = 0
        num = 0
        group_score = []
        for student in list_of_students:
            if student.group == group_number:
                grades = student.get_grades()
                for k in grades:
                    score += sum(grades[k])
                    num += len(grades[k])
                group_score.append(score/num)
        return f'group score of {group_number} group is {sum(group_score)/len(group_score)}'

    @staticmethod
    def get_average_score_class(list_of_students, subject):
        our_dict = dict()
        x = []
        for student in list_of_students:
            grades = student.get_grades()
            if subject in grades:
                our_dict[subject] = grades[subject]
                x.append(Student.get_score_by_grade(our_dict))
        return f'{subject} AVG score = {sum(x) / len(x)}'


Igor = Student('Igor', '4', {'math': [5, 5, 5], 'physic': [5, 5, 5], 'english': [5, 5, 5, 5]})
Oleg = Student('Oleg', '4', {'math': [3, 3, 3], 'physic': [3, 3, 3], 'english': [3, 3, 3, 3]})
print(Igor.get_average_score_student())
print(Student.get_average_score_group([Oleg, Igor], group_number='4'))
print(Student.get_average_score_class([Oleg, Igor], subject='english'))