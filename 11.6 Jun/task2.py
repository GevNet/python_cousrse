import itertools


def lazy_union(l1, l2):
    return itertools.chain(l1, l2)


print(list(lazy_union([1, 2], [3, 4])))