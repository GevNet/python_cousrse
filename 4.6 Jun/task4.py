def reverse(number):
    if number >= 0:
        return int(str(number)[::-1])
    else:
        return (-1) * int(str(number)[:0:-1])


if __name__ == "__main__":
    print(reverse(-1234567890))