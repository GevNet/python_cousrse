def rounds(some_float):
    x3 = str(some_float)
    if len(str(some_float)) < 11:
        for i in range(11 - len(str(some_float))):
            x3 = "0" + x3
    return [round(some_float, 2), round(some_float), x3]


if __name__ == "__main__":
    print(rounds(14.721))