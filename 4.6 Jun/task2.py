from random import randint


def delenie(x, y):
    first = x / y
    second = x % y
    return [first, second]


if __name__ == "__main__":
    print(delenie(177, 10))