def reverse(number):
    if abs(number) <  2147483647:
        if number >= 0:
            return int(str(number)[::-1])
        else:
            return (-1) * int(str(number)[:0:-1])
    else:
        return 0


if __name__ == "__main__":
    print(reverse(-123456789054645645))