def task1(list):
    if len(list) > 0:
        x = sum(list[i] for i in range(0, len(list), 2))
        return x * list[len(list)-1]
    else:
        return 0


if __name__ == "__main__":
    print(task1([]))