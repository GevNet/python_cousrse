def mediana(list):
    n = len(list)
    index = n // 2
    if n % 2:
        return sorted(list)[index]
    return sum(sorted(list)[index - 1:index + 1]) / 2


if __name__ == "__main__":
    print(mediana([3, 6, 20, 99, 10, 15]))