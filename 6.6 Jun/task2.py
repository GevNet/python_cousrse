def max_minus_min(list):
    if len(list) > 0:
        minimum = min(list)
        maximum = max(list)
        return '%.1f' % (maximum - minimum)
    else:
        return 0


if __name__ == "__main__":
    print(max_minus_min([-5, 5]))