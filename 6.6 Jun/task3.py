def sort(tuple):
    return sorted(tuple, key=abs)


if __name__ == "__main__":
    print(sort((-1, -2, -3, 0)))