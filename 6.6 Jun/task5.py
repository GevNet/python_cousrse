import re

def zebra_words(text):
    x1 = re.findall(r"\b([aeiouy][bcdfghjklmnpqrstvwxz])+\b", text, flags=re.IGNORECASE)
    x2 = re.findall(r"\b([bcdfghjklmnpqrstvwxz][aeiouy]){1,10}\b", text, flags=re.IGNORECASE)
    return len(x1)+len(x2)


if __name__ == "__main__":
    print(zebra_words("A quantity of striped words."))