import re


def find_int(string):
    x = re.match(r'([A-Za-z]+\s){2}[a-z]', string)
    if x is None:
        return False
    else:
        return True


if __name__ == "__main__":
    print(find_int("He 21 qew isn"))