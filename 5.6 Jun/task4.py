def up_case(some_string):
    secret_message = ""
    for i in some_string:
        if i.isupper():
            secret_message += i
    return secret_message


if __name__ == "__main__":
    print(up_case("How are you? Eh, ok. Low or Lower? Ohhh."))