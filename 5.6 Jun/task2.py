def grade(some_int):
    result = ("Плохо" if some_int % 2 == 1 else
              "Неплохо" if (2 <= some_int <= 5) else
              "Так себе" if (6 <= some_int <= 20) else
              "Отлично")
    return result


if __name__ == "__main__":
    print(grade(24))
