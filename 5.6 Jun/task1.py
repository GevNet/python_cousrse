def fizz_buzz(some_int):
    result = ("Fizz Buzz" if some_int % 3 == 0 and some_int % 5 == 0 else
              "Fizz" if some_int % 3 == 0 else
              "Buzz" if some_int % 5 == 0 else
              str(some_int))
    return result


if __name__ == "__main__":
    print(fizz_buzz(7))