import re


def lefthand(list_of_string):
    for i in range(len(list_of_string)):
        list_of_string[i] = re.sub(r'right', r"left", list_of_string[i])
    result = ','.join(list_of_string)
    return result


if __name__ == "__main__":
    print(lefthand(["bright aright", "ok"]))