def reverse_array(some_int):
    return [int(i) for i in str(some_int)[::-1]]


print(reverse_array(123))
