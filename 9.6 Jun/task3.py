def return_course(list_of_students):
    for i in list_of_students:
        if i['group_n'] == 223:
            yield i["last_name"]


def avg_grad(list_of_students):
    for i in list_of_students:
        yield sum(i['graduates']) / len(i['graduates'])


def the_oldest_and(list):
    a = []
    for i in list:
        a.append(i['year_born'])
    return min(a), max(a)


my_list = [{'last_name': 'Ivanov', 'first_name': 'Petr', 'middle_name': 'Victorovich', 'year_born': 1908,
            'course': 3, 'group_n': 223, 'graduates': [3, 4, 5, 3, 4]},
           {'last_name': 'Petrov', 'first_name': 'Ivan', 'middle_name': 'Olegovich', 'year_born': 1987,
            'course': 2, 'group_n': 223, 'graduates': [3, 5, 5, 3, 4]},
           {'last_name': 'Solovyev', 'first_name': 'Artem', 'middle_name': 'Igorevich', 'year_born': 1947,
            'course': 2, 'group_n': 333, 'graduates': [2, 2, 5, 3, 4]},
           {'last_name': 'Pavlov', 'first_name': 'Artemiy', 'middle_name': 'Fedorovich', 'year_born': 1937,
            'course': 2, 'group_n': 333, 'graduates': [1, 2, 3, 3, 4]}
           ]

print(list(return_course(my_list)))
print(list(avg_grad(my_list)))
print(the_oldest_and(my_list))

