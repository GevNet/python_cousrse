def double_number(n):
    return [i*2 for i in range(n) if i % 2 != 0]


if __name__ == "__main__":
    print(double_number(5))