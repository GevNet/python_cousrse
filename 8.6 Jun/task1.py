def ununique(some_array):
    return [i for i in some_array if some_array.count(i) > 1]


if __name__ == "__main__":
    print(ununique([10, 9, 10, 10, 9, 8, 3, 2, 2, 5]))