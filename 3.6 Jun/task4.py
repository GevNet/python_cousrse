def money(rubles):
    return '₽{:,.2f}'.format(rubles)


if __name__ == "__main__":
    print(money(100000))
